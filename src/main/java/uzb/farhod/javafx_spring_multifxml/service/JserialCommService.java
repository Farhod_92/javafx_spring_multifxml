package uzb.farhod.javafx_spring_multifxml.service;

import com.fazecast.jSerialComm.SerialPort;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.springframework.stereotype.Service;

import java.io.InputStream;

@Service
public class JserialCommService {

    public ObservableList<String> getAvailablePorts(){
        ObservableList<String> observableList = FXCollections.observableArrayList();
        SerialPort[] commPorts = SerialPort.getCommPorts();
        for (SerialPort commPort : commPorts) {
            System.out.print(commPort.getSystemPortName() + "  " );
            observableList.add(commPort.getSystemPortName());
        }
        return observableList;
    }

    public void send(String port, byte[] bytesToSend) {

        try {
            SerialPort comPort = SerialPort.getCommPort(port);
            comPort.setBaudRate(115200);
            comPort.openPort();
            comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING,0,0);
            InputStream inputStream=comPort.getInputStream();

            int i = comPort.writeBytes(bytesToSend, bytesToSend.length);
            System.out.println("output length= " +  i);
            for (byte inputByte : bytesToSend) {
                System.out.print("0x"+Integer.toHexString(inputByte & 0xFF) + ",");
            }


            //while (true){
                byte[] inputBytes=new byte[inputStream.available()];
                int read = comPort.readBytes(inputBytes,inputBytes.length);
                System.out.print("input length= "+ inputBytes.length + " : ");
                System.out.print("read= "+read);
           // }

                //inputStream.close();
                //comPort.closePort();

        } catch (Exception ioException) {
            ioException.printStackTrace();
        }
    }
}