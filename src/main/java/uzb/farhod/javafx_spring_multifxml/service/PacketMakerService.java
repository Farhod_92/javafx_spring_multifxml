package uzb.farhod.javafx_spring_multifxml.service;
import org.springframework.stereotype.Service;

@Service
public class PacketMakerService {

   public byte[] makePacket(byte[] dataBytes){
       int length = dataBytes.length + 6;
       byte[] checksumBytes = calculateChecksum(dataBytes);
       byte[] readyPacketBytes = new byte[length];
       byte[] packetLength = longToByteArray(length);
       //byte[] packetLength = ByteBuffer.allocate(4).putInt(length).array();

       readyPacketBytes[0] = (byte)0xc0;
       //TODO packetLength arrayidagi indexlari almashtirilgan
       readyPacketBytes[1] = packetLength[1];
       readyPacketBytes[2] = packetLength[0];
       readyPacketBytes[3] = checksumBytes[0];
       readyPacketBytes[4] = checksumBytes[1];
       System.arraycopy(dataBytes, 0, readyPacketBytes, 5, dataBytes.length);
       readyPacketBytes[length-1] = (byte)0xc1;
       return readyPacketBytes;
   }

    byte[] calculateChecksum(byte[] buf) {
        int length = buf.length;
        int i = 0;
        long sum = 0;
        while (length > 0) {
            sum += (buf[i++]&0xff) << 8;
            if ((--length)==0) break;
            sum += (buf[i++]&0xff);
            --length;
        }

       long l =  (~((sum & 0xFFFF)+(sum >> 16)))&0xFFFF;
        return longToByteArray(l);
    }

    byte[] longToByteArray(long l){
        byte[] chs = new byte[2];
        for (int k = 0; k < 2; ++k) {
            chs[k] = (byte) (l >> (2 - k - 1 << 3));
        }
        return chs;
    }
}