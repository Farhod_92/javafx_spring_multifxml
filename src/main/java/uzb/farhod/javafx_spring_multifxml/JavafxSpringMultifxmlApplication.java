package uzb.farhod.javafx_spring_multifxml;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class JavafxSpringMultifxmlApplication extends Application {

    private ConfigurableApplicationContext springContext;
    private Parent rootNode;

    public static void main(String[] args) {
        //SpringApplication.run(JavafxSpringMultifxmlApplication.class, args);
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setScene(new Scene(rootNode, 700, 400));
        stage.show();
    }

    @Override
    public void init() throws Exception {
        springContext = SpringApplication.run(JavafxSpringMultifxmlApplication.class);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/views/MainWindow.fxml"));
        fxmlLoader.setControllerFactory(springContext::getBean);
        rootNode = fxmlLoader.load();
    }

    @Override
    public void stop() throws Exception {
        springContext.close();
    }
}


//https://drogago.net/javafxmavenspring-step-by-step/
// https://www.youtube.com/watch?v=k93C-CZ6f-A