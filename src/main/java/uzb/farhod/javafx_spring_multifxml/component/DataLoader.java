package uzb.farhod.javafx_spring_multifxml.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import uzb.farhod.javafx_spring_multifxml.entity.Users;
import uzb.farhod.javafx_spring_multifxml.repository.UsersRepository;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.sql.init.mode}")
    private String initialMode;

    @Autowired
    UsersRepository usersRepository;

    @Override
    public void run(String... args) throws Exception {
        if(initialMode.equals("always")){
            Users user =new Users();
            user.setPassword("root");
            user.setUsername("root");
            usersRepository.save(user);
        }

    }
}
