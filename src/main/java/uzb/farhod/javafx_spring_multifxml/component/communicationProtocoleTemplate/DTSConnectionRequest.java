package uzb.farhod.javafx_spring_multifxml.component.communicationProtocoleTemplate;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uzb.farhod.javafx_spring_multifxml.service.PacketMakerService;

import java.util.Random;

@Data
@Component
public class DTSConnectionRequest {
    
    @Autowired
    PacketMakerService packetMakerService;
    
    private final byte[] connectionId = new byte[2];

    public DTSConnectionRequest() {
      new Random().nextBytes(connectionId);
    }

    public byte[] getBytes(){
        byte[] dataBytes = new byte[17];
        dataBytes[0] = 0x09;
        dataBytes[1] = 0x01;
        dataBytes[2] = 0x0d;
        dataBytes[3] = 0x00;
        dataBytes[4] = 0x00;
        dataBytes[5] = 0x08;
        dataBytes[6] = 0x00;
        dataBytes[7] = 0x00;
        dataBytes[8] = 0x00;
        dataBytes[9] = 0x00;
        dataBytes[10] = 0x01;
        dataBytes[11] = 0x01;
        dataBytes[12] = 0x00;
        dataBytes[13] = 0x00;
        dataBytes[14] = 0x01;
        dataBytes[15] = connectionId[0];
        dataBytes[16] = connectionId[1];

        return  packetMakerService.makePacket(dataBytes);
    }

}
