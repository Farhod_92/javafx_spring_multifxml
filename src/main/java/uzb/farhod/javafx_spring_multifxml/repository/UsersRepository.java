package uzb.farhod.javafx_spring_multifxml.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.javafx_spring_multifxml.entity.Users;

public interface UsersRepository extends JpaRepository<Users, Integer> {
}
