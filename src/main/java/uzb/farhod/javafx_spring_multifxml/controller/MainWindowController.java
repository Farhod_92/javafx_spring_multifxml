package uzb.farhod.javafx_spring_multifxml.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class MainWindowController {

    @FXML
    private AnchorPane connectionAnchor;

    @FXML
    private AnchorPane fileAnchor;

    @FXML
    private AnchorPane settingsAnchor;

    @FXML
    public void initialize() throws IOException {
//        connectionAnchor.getChildren().clear();
//        Pane connectionPane = FXMLLoader.load(getClass().getResource("/views/Test.fxml"));
//        connectionAnchor.getChildren().add(connectionPane);
//
//        fileAnchor.getChildren().clear();
//        Pane filePane = FXMLLoader.load(getClass().getResource("/views/File.fxml"));
//        fileAnchor.getChildren().add(filePane);
//
//        settingsAnchor.getChildren().clear();
//        Pane settingsPane = FXMLLoader.load(getClass().getResource("/views/Settings.fxml"));
//        settingsAnchor.getChildren().add(settingsPane);
    }
}
