package uzb.farhod.javafx_spring_multifxml.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uzb.farhod.javafx_spring_multifxml.entity.Users;
import uzb.farhod.javafx_spring_multifxml.repository.UsersRepository;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

@Component
public class TestController implements Initializable{
    @Autowired
    private UsersRepository usersRepository;

    @FXML
    private Button button;

    private List<Users> users;

    public void setExtends(List<Users> all){
        this.users = all;
        System.out.println("Extended values:" + users.get(0).getUsername());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        button.setOnAction(event -> {
            System.out.println(usersRepository.findAll().get(0).getUsername());
        });
    }

}
