package uzb.farhod.javafx_spring_multifxml.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uzb.farhod.javafx_spring_multifxml.component.communicationProtocoleTemplate.DTSConnectionRequest;

import java.net.URL;
import java.util.ResourceBundle;

@Component
public class SettingsPaneController implements Initializable {
    @Autowired
    DTSConnectionRequest dtsConnectionRequest;

    @FXML private Button button;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        button.setOnAction(event -> {
            for (byte inputByte : dtsConnectionRequest.getBytes()) {
                System.out.print("0x"+Integer.toHexString(inputByte & 0xFF) + ",");
            }
            System.out.println("\n");

        });
    }
}
