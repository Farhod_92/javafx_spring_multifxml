package uzb.farhod.javafx_spring_multifxml.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uzb.farhod.javafx_spring_multifxml.component.communicationProtocoleTemplate.DTSConnectionRequest;
import uzb.farhod.javafx_spring_multifxml.service.JserialCommService;


@Component
public class ConnectionPaneController {

    @Autowired
    JserialCommService jserialCommService;

    @Autowired
    DTSConnectionRequest dtsConnectionRequest;


    @FXML
    public Label label;

    @FXML
    public Button button;

    @FXML
    public Button checkBut;

    @FXML
    public ComboBox<String> portsCombo;

    @FXML
    public void initialize(){

        portsCombo.setItems(jserialCommService.getAvailablePorts());
        portsCombo.valueProperty().addListener((observable, oldVal, newVal )-> {
            label.setText("selected port: " + newVal);
        });

        button.setOnAction(event -> {

            for (byte inputByte : dtsConnectionRequest.getBytes()) {
                System.out.print("0x"+Integer.toHexString(inputByte & 0xFF) + ",");
            }

            jserialCommService.send(portsCombo.getValue(), dtsConnectionRequest.getBytes());
        });

        checkBut.setOnAction(event -> {
            for (byte inputByte : dtsConnectionRequest.getConnectionId()) {
                System.out.print("0x"+Integer.toHexString(inputByte & 0xFF) + ",");
            }
        });
    }
}
