package uzb.farhod.javafx_spring_multifxml.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import uzb.farhod.javafx_spring_multifxml.repository.UsersRepository;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

@Component
public class FilePaneController implements Initializable {

    @Autowired
    private ConfigurableApplicationContext springContext;

    @Autowired
    UsersRepository usersRepository;

    @FXML
    private Button button;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        button.setOnAction(event -> {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/Test.fxml"));
                loader.setControllerFactory(springContext::getBean); //SAMIY KLYUCH
                Parent root = loader.load();
                TestController testController = loader.getController();
                testController.setExtends(usersRepository.findAll());

                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setScene(scene);
                stage.setResizable(false);
                //stage.getIcons().add(new Image(this.getClass().getResource("img/logo.png").toString()));
                stage.setResizable(true);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setOnCloseRequest(ev -> {
                    System.out.println("Test stage closed");
                });
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
